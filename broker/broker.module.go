package broker

import (
	"server1/broker/cashflow"
	"server1/broker/mediator"
)

type BrokerModule struct {
	Cashflow *cashflow.CashflowService
}

//

func CreateModule() *BrokerModule {
	mediator := mediator.CreateMediator([]string{"kafka1:9092", "kafka2:9092"})

	cashflowService := cashflow.CreateService(mediator, "cashflow2", 10)

	return &BrokerModule{cashflowService}
}

func (broker *BrokerModule) Close() {
}
