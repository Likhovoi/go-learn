package cashflow

import (
	"context"
	"encoding/json"
	"server1/broker/mediator"
	"time"

	"github.com/segmentio/kafka-go"
	log "github.com/sirupsen/logrus"
)

type CashflowService struct {
	mediator   *mediator.Mediator
	topic      string
	partitions int
}

func CreateService(brokerConfig *mediator.Mediator, topic string, partitions int) *CashflowService {
	return &CashflowService{brokerConfig, topic, partitions}
}

//

func (service *CashflowService) Write(input *CashflowMessageData) error {
	bytes, err := json.Marshal(input)

	if err != nil {
		return err
	}

	topicWriter := service.mediator.NewWriter(service.topic)

	err = topicWriter.WriteMessages(context.Background(), kafka.Message{
		Value:     bytes,
		Partition: input.UserId % service.partitions,
	})

	if err != nil {
		log.Error("Failed to write message:", err)
		return err
	}

	return err
}

//

func (service *CashflowService) Read(queue chan<- CashflowJob, stop <-chan bool) {
	reader := service.mediator.NewGroupReader(service.topic, "txs-group0")

	// cg, err := service.mediator.CreateConsumerGroup("txs-group", service.topic)

	// if err != nil {
	// 	log.Error(err)
	// }

	next := make(chan bool)

	go func() {
		for {
			select {
			case <-next:
				log.Info("Next")
				service.Read(queue, stop)
				return
			case <-stop:
				log.Info("Stopped")
				return
			default:
				log.Info("Fetch message")

				ctx, cancel := context.WithCancel(context.Background())

				go func() {
					time.Sleep(time.Second * 31)
					cancel()
				}()

				msg, err := reader.FetchMessage(ctx)

				if err != nil {
					log.Error(err)
					continue
				}

				job := CreateCashflowJobFromMessage(&msg)

				go func() {
					log.Info("Put new job", string(msg.Time.String()))
					queue <- job
				}()

				for committed := false; committed == false; {
					select {
					case <-job.CommitAsked:
						log.Info("Commit")
						err = reader.CommitMessages(context.Background(), msg)
						if err != nil {
							log.Error(err)
						}
						committed = true
						close(next)
						break
					}
				}

			}

		}
	}()
}
