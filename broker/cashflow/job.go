package cashflow

import (
	"encoding/json"

	"github.com/segmentio/kafka-go"
)

type CashflowMessageData struct {
	UserId   int     `json:"userId"`
	Amount   float64 `json:"amount"`
	Currency string  `json:"currency"`
}

type CashflowMessage struct {
	data    *CashflowMessageData
	Version string `json:"version"`
}

type CashflowJob struct {
	Message     *CashflowMessage
	CommitAsked chan bool
}

func CreateCashflowJobFromMessage(rawMessage *kafka.Message) CashflowJob {
	var message CashflowMessage

	json.Unmarshal(rawMessage.Value, &message)

	ch := make(chan bool, 1)

	return CashflowJob{&message, ch}
}

func (job *CashflowJob) Commit() {
	close(job.CommitAsked)
}
