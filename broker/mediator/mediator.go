package mediator

import (
	"context"
	"time"

	"github.com/segmentio/kafka-go"
)

type MsgSrcBalancer struct {
}

func (b *MsgSrcBalancer) Balance(msg kafka.Message, partitions ...int) int {
	return msg.Partition
}

type Mediator struct {
	dialer  *kafka.Dialer
	brokers []string
}

func (mediator *Mediator) NewWriter(topic string) *kafka.Writer {
	return kafka.NewWriter(kafka.WriterConfig{
		Dialer:       mediator.dialer,
		Brokers:      mediator.brokers,
		Topic:        topic,
		WriteTimeout: 2 * time.Second,
		Balancer:     &MsgSrcBalancer{},
	})
}

func (mediator *Mediator) CreateConsumerGroup(group string, topic string) (*kafka.ConsumerGroup, error) {
	return kafka.NewConsumerGroup(kafka.ConsumerGroupConfig{
		ID:                group,
		Brokers:           mediator.brokers,
		Dialer:            mediator.dialer,
		Topics:            []string{topic},
		Timeout:           time.Second * 3,
		HeartbeatInterval: time.Second * 3,
	})
}

func (mediator *Mediator) NewGroupReader(topic string, group string) *kafka.Reader {
	return kafka.NewReader(kafka.ReaderConfig{
		Brokers:  mediator.brokers,
		Topic:    topic,
		GroupID:  group,
		MinBytes: 10e3, // 10KB
		MaxBytes: 10e6, // 10MB
	})
}

func (mediator *Mediator) LookupPartitions(topic string) ([]kafka.Partition, error) {
	// TODO
	partitions, err := kafka.LookupPartitions(context.Background(), "tcp", mediator.brokers[0], topic)

	if err != nil {
		partitions, err = kafka.LookupPartitions(context.Background(), "tcp", mediator.brokers[1], topic)
	}

	return partitions, err
}

func CreateMediator(brokers []string) *Mediator {
	dialer := &kafka.Dialer{
		Timeout:   2 * time.Second,
		DualStack: true,
	}

	return &Mediator{dialer, brokers}
}
