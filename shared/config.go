package shared

import (
	"fmt"
	"os"
	"strings"
)

func GetDatabaseUrl() string {
	postgreHost := os.Getenv("POSTGRESQL_HOST")
	postgreUser := os.Getenv("POSTGRESQL_USERNAME")
	postgrePass := os.Getenv("POSTGRESQL_PASSWORD")
	postgreDb := os.Getenv("POSTGRESQL_DATABASE")
	return fmt.Sprintf("postgres://%s:%s@%s:5432/%s?sslmode=disable", postgreUser, postgrePass, postgreHost, postgreDb)
}

func GetJwtSecret() string {
	return os.Getenv("JWT_SECRET")
}

func GetJwtExpirationMinutes() int {
	return 60 * 12
}

func GetKafkaBrokers() []string {
	brokersStr := os.Getenv("KAFKA_BROKERS")
	return strings.Split(brokersStr, ",")
}
