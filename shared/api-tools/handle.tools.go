package apitools

import (
	"encoding/json"
	"net/http"

	validator "github.com/asaskevich/govalidator"
)

type ResponseMap = map[string]interface{}

//

func FillDtoFromRequest(r *http.Request, dto interface{}) (bool, *ResponseMap) {
	err := json.NewDecoder(r.Body).Decode(&dto)

	if err != nil {
		return false, nil
	}

	_, err = validator.ValidateStruct(dto)

	if err != nil {
		responseMap := CastValidationErrorsToResponseMap(err.(validator.Errors))
		return false, responseMap
	}

	return true, nil
}

func WriteSingleFieldError(w http.ResponseWriter, field string, key []string) {
	responseMap := ResponseMap{}
	responseMap[field] = key
	WriteJsonMap(http.StatusBadRequest, w, &responseMap)
}

func WriteJsonMap(status int, w http.ResponseWriter, data *ResponseMap) {
	if data == nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	bytes, err := json.Marshal(data)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(status)
	w.Write(bytes)
}

func CastValidationErrorsToResponseMap(errors validator.Errors) *ResponseMap {
	responseMap := ResponseMap{}

	for _, entry := range errors {
		validEntry := entry.(validator.Error)

		if _, ok := responseMap[validEntry.Name]; ok {
			_ = append(responseMap[validEntry.Name].([]string), validEntry.Validator)
		} else {
			responseMap[validEntry.Name] = []string{validEntry.Validator}
		}
	}

	return &responseMap
}
