package apitools

import (
	"context"
	"errors"
	"net/http"
	"server1/shared"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	log "github.com/sirupsen/logrus"
)

var UnauthorizedError = errors.New("Unauthorized")

var InvalidTokenError = errors.New("Invalid token")

type AuthTokenClaimInput struct {
	UserId    int    `json:"userId"`
	UserEmail string `json:"userEmail"`
}

type AuthTokenClaim struct {
	*jwt.StandardClaims
	*AuthTokenClaimInput
}

//

func SetUserToContext(ctx context.Context, claims AuthTokenClaim) context.Context {
	return context.WithValue(ctx, "auth", claims)
}

func MustGetUserFromContext(ctx context.Context) *AuthTokenClaim {
	return GetUserFromContext(ctx, false)
}

func GetUserFromContext(ctx context.Context, optional bool) *AuthTokenClaim {
	value := ctx.Value("auth")

	if value == "" {
		return nil
	}

	if !optional && value == nil {
		panic(UnauthorizedError)
	}

	claim := value.(AuthTokenClaim)

	return &claim
}

//

func Guard(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		authHeader := r.Header.Get("authorization")

		if authHeader == "" {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		claims, err := ParseJwtToken(authHeader)

		if err != nil {
			log.Error(err)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		authContext := SetUserToContext(r.Context(), *claims)

		handler(w, r.WithContext(authContext))
	}
}

func SignJwtToken(input *AuthTokenClaimInput) (string, error) {
	jwtSecret := shared.GetJwtSecret()
	jwtExpire := time.Now().Add(time.Minute * time.Duration(shared.GetJwtExpirationMinutes())).Unix()

	claims := &AuthTokenClaim{
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: jwtExpire,
			Issuer:    "api",
		},
		AuthTokenClaimInput: input,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return token.SignedString([]byte(jwtSecret))
}

func ParseJwtToken(authHeader string) (*AuthTokenClaim, error) {
	jwtSecret := shared.GetJwtSecret()

	parser := new(jwt.Parser)

	token, err := parser.ParseWithClaims(authHeader, &AuthTokenClaim{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(jwtSecret), nil
	})

	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(*AuthTokenClaim)

	if !ok || !token.Valid {
		return nil, InvalidTokenError
	}

	return claims, nil
}
