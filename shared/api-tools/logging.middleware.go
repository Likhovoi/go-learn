package apitools

import (
	"fmt"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
)

func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Info(fmt.Sprintf(`New request '%s %s'`, r.Method, r.RequestURI))
		start := time.Now()
		next.ServeHTTP(w, r)
		log.Info(fmt.Sprintf(`Request '%s %s' took %s`, r.Method, r.RequestURI, time.Since(start)))
	})
}
