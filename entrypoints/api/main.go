package main

import (
	"server1/api"
	"server1/broker"
	"server1/db"

	log "github.com/sirupsen/logrus"
)

func main() {
	log.SetFormatter(&log.JSONFormatter{})

	dbModule := db.CreateModule()

	defer dbModule.Close()

	brokerModule := broker.CreateModule()

	defer brokerModule.Close()

	apiModule := api.CreateModule(dbModule, brokerModule)

	defer apiModule.Close()

	apiModule.Listen()
}
