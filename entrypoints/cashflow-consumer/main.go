package main

import (
	"server1/broker"
	"server1/broker/cashflow"
	"server1/db"
	"time"

	log "github.com/sirupsen/logrus"
)

func main() {
	log.SetFormatter(&log.JSONFormatter{})

	dbModule := db.CreateModule()
	defer dbModule.Close()

	brokerModule := broker.CreateModule()
	defer brokerModule.Close()

	jobs := make(chan cashflow.CashflowJob)
	defer close(jobs)

	stop := make(chan bool)
	defer close(stop)

	brokerModule.Cashflow.Read(jobs, stop)

	for job := range jobs {
		go func(value cashflow.CashflowJob) {
			time.Sleep(1)
			value.Commit()
		}(job)
	}
}
