package common

import (
	"net/http"
	apitools "server1/shared/api-tools"
)

func (module *ApiModule) HealthCheck(w http.ResponseWriter, r *http.Request) {
	apitools.WriteJsonMap(http.StatusOK, w, &apitools.ResponseMap{
		"alive": true,
	})
}
