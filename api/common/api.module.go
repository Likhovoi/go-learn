package common

import (
	"github.com/gorilla/mux"
)

type ApiModule struct {
}

func CreateModule() *ApiModule {
	return &ApiModule{}
}

func (module *ApiModule) AttachRoutes(router *mux.Router) {
	router.HandleFunc("/health-check", module.HealthCheck)
}
