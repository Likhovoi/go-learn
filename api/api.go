package api

import (
	"net/http"
	apicommon "server1/api/common"
	apiv1 "server1/api/v1"
	"server1/broker"
	"server1/db"
	apitools "server1/shared/api-tools"
	"time"

	mux "github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

type ApiModule struct {
	v1     *apiv1.ApiModule
	common *apicommon.ApiModule
	server *http.Server
}

func CreateModule(db *db.DbModule, brokerModule *broker.BrokerModule) *ApiModule {
	apiV1 := apiv1.CreateModule(db, brokerModule)
	apiCommon := apicommon.CreateModule()
	apiModule := &ApiModule{apiV1, apiCommon, nil}

	return apiModule
}

func (api *ApiModule) Listen() {
	router := mux.NewRouter()

	router.Use(apitools.LoggingMiddleware)

	router.Use(apitools.ErrorHandlerMiddleware)

	api.v1.AttachRoutes(router.PathPrefix("/api/v1/").Subrouter())

	api.common.AttachRoutes(router.PathPrefix("/").Subrouter())

	log.Info("Api listening on port 80")

	server := &http.Server{
		Handler:      router,
		Addr:         ":80",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	api.server = server

	log.Fatal(server.ListenAndServe())
}

func (api *ApiModule) Close() {
	if api != nil {
		api.server.Close()
	}
}
