package auth

import (
	"net/http"
	"server1/db/repositories"
	security "server1/shared"
	apitools "server1/shared/api-tools"
)

type SignUpDto struct {
	Email    string `valid:"required,email" json:"email"`
	Password string `valid:"required,minstringlength(3),maxstringlength(50)" json:"password"`
}

func (module *AuthModule) SignUp(w http.ResponseWriter, r *http.Request) {
	var dto SignUpDto

	ok, responseMap := apitools.FillDtoFromRequest(r, &dto)

	if !ok {
		apitools.WriteJsonMap(http.StatusBadRequest, w, responseMap)
		return
	}

	existedUser := module.userRepo.FindByEmail(dto.Email)

	if existedUser != nil {
		w.WriteHeader(http.StatusConflict)
		return
	}

	_ = module.userRepo.CreateUser(&repositories.UserPayload{
		Email:        dto.Email,
		PasswordHash: security.HashString(dto.Password),
	})

	w.WriteHeader(http.StatusCreated)
}
