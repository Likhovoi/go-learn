package auth

import (
	"net/http"
	"server1/shared"
	apitools "server1/shared/api-tools"

	log "github.com/sirupsen/logrus"
)

type SignInDto struct {
	Email    string `valid:"required,email" json:"email"`
	Password string `valid:"required,minstringlength(3),maxstringlength(50)" json:"password"`
}

func (module *AuthModule) SignIn(w http.ResponseWriter, r *http.Request) {
	var dto SignInDto

	ok, responseMap := apitools.FillDtoFromRequest(r, &dto)

	if !ok {
		apitools.WriteJsonMap(http.StatusBadRequest, w, responseMap)
		return
	}

	passwordHash := shared.HashString(dto.Password)

	user := module.userRepo.FindByEmail(dto.Email)

	if user == nil || user.PasswordHash != passwordHash {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	if !user.IsActive() {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	token, err := apitools.SignJwtToken(&apitools.AuthTokenClaimInput{
		UserId:    user.Id,
		UserEmail: user.Email,
	})

	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	apitools.WriteJsonMap(http.StatusOK, w, &apitools.ResponseMap{
		"token": token,
	})
}
