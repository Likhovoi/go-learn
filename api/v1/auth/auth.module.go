package auth

import (
	repositories "server1/db/repositories"
)

type AuthModule struct {
	userRepo *repositories.UserRepository
}

func CreateAuthModule(userRepo *repositories.UserRepository) *AuthModule {
	module := &AuthModule{userRepo}

	return module
}
