package auth

import (
	"net/http"
	apitools "server1/shared/api-tools"
)

func (module *AuthModule) GetSigned(w http.ResponseWriter, r *http.Request) {
	auth := apitools.MustGetUserFromContext(r.Context())

	apitools.WriteJsonMap(http.StatusOK, w, &apitools.ResponseMap{
		"id":    auth.UserId,
		"email": auth.UserEmail,
	})
}
