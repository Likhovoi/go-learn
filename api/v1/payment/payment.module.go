package payment

import "server1/broker"

type PaymentModule struct {
	broker *broker.BrokerModule
}

func CreateModule(broker *broker.BrokerModule) *PaymentModule {
	return &PaymentModule{broker}
}
