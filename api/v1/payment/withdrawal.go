package payment

import (
	log "github.com/sirupsen/logrus"

	"net/http"
	"server1/broker/cashflow"
)

func (module *PaymentModule) Withdrawal(w http.ResponseWriter, r *http.Request) {
	err := module.broker.Cashflow.Write(&cashflow.CashflowMessageData{
		UserId:   1,
		Amount:   -0.01,
		Currency: "usd",
	})

	if err != nil {
		log.Error(err)
		w.WriteHeader(500)
		return
	}

	w.WriteHeader(201)
}
