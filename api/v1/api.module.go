package v1

import (
	auth "server1/api/v1/auth"
	"server1/api/v1/payment"
	"server1/broker"
	db "server1/db"
	apitools "server1/shared/api-tools"

	mux "github.com/gorilla/mux"
)

type ApiModule struct {
	auth    *auth.AuthModule
	payment *payment.PaymentModule
}

func CreateModule(db *db.DbModule, brokerModule *broker.BrokerModule) *ApiModule {
	auth := auth.CreateAuthModule(db.UserRepository)
	payment := payment.CreateModule(brokerModule)

	v1 := &ApiModule{auth, payment}

	return v1
}

func (v1 *ApiModule) AttachRoutes(router *mux.Router) {
	router.HandleFunc("/auth/sign-in", v1.auth.SignIn).Methods("POST")
	router.HandleFunc("/auth/sign-up", v1.auth.SignUp).Methods("POST")
	router.HandleFunc("/auth", apitools.Guard(v1.auth.GetSigned)).Methods("GET")
	router.HandleFunc("/payment/deposit", apitools.Guard(v1.payment.Deposit)).Methods("POST")
	router.HandleFunc("/payment/withdrawal", apitools.Guard(v1.payment.Withdrawal)).Methods("POST")
}
