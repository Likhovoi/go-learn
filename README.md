# Go app example

## Run application locally

```
docker-compose up api
```

## Run migrations

```
docker-compose run migrations app migrate:up
```

## Generate new migration

```
docker-compose run migrations app migrate:new create_users_table
```

## Kafka init

Before entering the command ensure your first kafka broker is up and running

```
docker-compose up -d kafka1
```

Then execute:

```
docker-compose run kafka2 app init
```
