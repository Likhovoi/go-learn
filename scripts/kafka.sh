#!/bin/sh

if [[ $1 == "init" ]]
then
    kafka-topics \
        --bootstrap-server kafka1:9092 \
        --topic cashflow \
        --create \
        --replication-factor 1 \
        --partitions 10
fi