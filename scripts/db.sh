#!/bin/sh

pgurl=postgres://${POSTGRESQL_USERNAME}:${POSTGRESQL_PASSWORD}@${POSTGRESQL_HOST}:5432/${POSTGRESQL_DATABASE}?sslmode=disable
migrationsPath=/src/app/migrations

echo $pgurl

if [[ $1 == "migrate:up" ]]
then
    migrate -path $migrationsPath -database $pgurl up
elif [[ $1 == "migrate:down" ]]
then
    migrate -path $migrationsPath -database $pgurl down
elif [[ $1 == "migrate:new" ]]
then
    migrate create -ext sql -dir $migrationsPath $2
fi