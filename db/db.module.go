package db

import (
	repos "server1/db/repositories"

	"github.com/jmoiron/sqlx"
)

type DbModule struct {
	conn           *sqlx.DB
	UserRepository *repos.UserRepository
}

func CreateModule() *DbModule {
	conn := InitConnection()

	module := &DbModule{
		UserRepository: repos.CreateUserRepository(conn),
	}

	return module
}

func (db *DbModule) Close() {
	db.conn.Close()
}
