package models

import "time"

type UserModel struct {
	Id           int       `db:"id"`
	Email        string    `db:"email"`
	PasswordHash string    `db:"password_hash"`
	IsRoot       bool      `db:"is_root"`
	Status       string    `db:"status"`
	CreatedAt    time.Time `db:"created_at"`
}

func (user *UserModel) IsActive() bool {
	return user.Status == "active"
}
