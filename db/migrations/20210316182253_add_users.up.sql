CREATE TYPE user_status AS ENUM('inactive', 'active');

CREATE TABLE "users" (
  "id" SERIAL PRIMARY KEY,
  "email" varchar NOT NULL,
  "password_hash" varchar NOT NULL,
  "is_root" boolean NOT NULL DEFAULT false,
  "status" user_status NOT NULL DEFAULT 'inactive',
  "created_at" timestamp NOT NULL DEFAULT (now())
);

CREATE INDEX ON "users" ("email");