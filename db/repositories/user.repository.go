package repositories

import (
	"database/sql"
	models "server1/db/models"

	"github.com/jmoiron/sqlx"
)

type UserPayload struct {
	Email        string `db:"email"`
	PasswordHash string `db:"password_hash"`
}

type UserRepository struct {
	conn *sqlx.DB
}

func CreateUserRepository(conn *sqlx.DB) *UserRepository {
	return &UserRepository{conn}
}

func (repo *UserRepository) FindUserById(id int) *models.UserModel {
	user := &models.UserModel{}

	err := repo.conn.Get(&user, "SELECT * FROM users WHERE id = ?", id)

	if err == sql.ErrNoRows {
		return nil
	}

	if err != nil {
		panic(err)
	}

	return user
}

func (repo *UserRepository) CreateUser(payload *UserPayload) *models.UserModel {
	res, err := repo.conn.NamedQuery("INSERT INTO users (email, password_hash) VALUES(:email, :password_hash) RETURNING *", payload)

	if err != nil {
		panic(err)
	}

	user := &models.UserModel{}

	if res.Next() {
		err = res.StructScan(&user)

		if err != nil {
			panic(err)
		}

		return user
	}

	panic("Could not insert user")
}

func (repo *UserRepository) FindByEmail(email string) *models.UserModel {
	user := &models.UserModel{}

	err := repo.conn.Get(user, "SELECT * FROM users WHERE email = $1", email)

	if err == sql.ErrNoRows {
		return nil
	}

	if err != nil {
		panic(err)
	}

	return user
}
