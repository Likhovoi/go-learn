package db

import (
	"fmt"

	shared "server1/shared"

	sqlx "github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
)

func InitConnection() *sqlx.DB {
	db, err := sqlx.Open("postgres", shared.GetDatabaseUrl())

	if err != nil {
		log.Fatal(err)
	}

	var pqNow string
	err = db.QueryRow("SELECT NOW()").Scan(&pqNow)

	if err != nil {
		log.Fatal(err)
	}

	log.Info(fmt.Sprintf("Postgres connected. Database time: %s", pqNow))

	return db
}
